import java.util.Scanner;

public class Ex4 {
    public static Scanner in = new Scanner(System.in);
    public static long[] generateVector(long[] vector){
        for (int i = 0; i < vector.length ; i++) {
            vector[i] = (long)(Math.random()*100)+1;
            System.out.print(vector[i] + " ");
        }
        return vector;
    }
    public static void afisareVector(long[] vector){
        for (long l : vector) {
            System.out.print(l + " ");
        }
    }
    public static long[] evenNumbers(long[] vector){
        int counter = 0;
        for (long l : vector) {
            if (l % 2 == 0) {
                counter++;
            }
        }
        long[] array = new long[counter];
        counter = 0;
        for (long l : vector) {
            if (l % 2 == 0) {
                array[counter] = l;
                counter++;
            }
        }
        return array;
    }
    public static void afiseazaMedia(long[] vector){
        int sum = 0;
        for (long l : vector) {
            sum += l;
        }
        System.out.println(sum/vector.length);
    }
    public static void main(String[] args) {
        int dimensiune = Integer.parseInt(args[0]); //run the program from terminal using command line "java program.java dimensiune"
        long[] array = new long[dimensiune];
        generateVector(array);
        System.out.println();
        afisareVector(evenNumbers(array));
        System.out.println();
        afiseazaMedia(array);
    }
}
