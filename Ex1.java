import java.util.Scanner;

public class Ex1 {
    public static Scanner in = new Scanner(System.in);
    public static int counter = 1;
    public static void displayMessage(String Message){ System.out.println(Message); }
    public static void initVector(int dim, int[] vector, char b){
        for (int i = 0; i < dim ; i++) {
            System.out.print(b + "["+i+"]= ");
            vector[i] = in.nextInt();
        }
    }
    public static void afisareVector(int[] vector1, int[] vector2){
        for (int value : vector1) {
            System.out.print(value + " ");
        }
        displayMessage("");
        counter++;
        if(counter ==2){
            afisareVector(vector2, vector1);
        }
    }
    public static void repeatIfNecessary(){
        displayMessage("Da-ti dimensiuniea primul vector de la tastatura: ");
        int dim1 = in.nextInt();
        displayMessage("Da-ti dimensiuniea celui de-al doilea vector de la tastatura: ");
        int dim2 = in.nextInt();
        dim1 = validareDimensiuni(dim1, dim2);
        int[] vector1 = new int[dim1];
        int[] vector2 = new int[dim2];
        initVector(dim1, vector1,'a');
        displayMessage(" ");
        initVector(dim2, vector2,'b');
        afisareVector(vector1,vector2);
        displayMessage(" ");
        checkIfTheSame(vector1,vector2,dim1);
    }
    public static int validareDimensiuni(int dim1, int dim2){
        if(dim1!=dim2){
            displayMessage("Da-ti o dimensiune egala cu " + dim2);
            dim1 = in.nextInt();
            displayMessage("Noua valoare este " + dim1);
            validareDimensiuni(dim1, dim2);
        }
        return dim1;
    }
    public static void checkIfTheSame(int[] vector1, int[] vector2, int dim1){
        int truecount = 0;
        for (int i = 0; i < dim1 ; i++) {
            boolean h = false;
            if(vector1[i]==vector2[i]){
                h = true;
                truecount++;
                if(truecount==dim1){
                    displayMessage("Valorile vectorilor sunt egale!");
                }
            }
        }
        if(truecount!=dim1){ displayMessage("Valorile nu sunt egale");}
    }
    public static void main(String[] args) {
        String R = "Yes";
        while(R.contains("Yes")){
            repeatIfNecessary();
            displayMessage("If you wish to continue please type \"Yes\" (case sensitive!).");
            R = in.nextLine();
        }
    }
}
