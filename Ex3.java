import java.util.Scanner;

public class Ex3 {
    public static Scanner in = new Scanner(System.in);
    public static void displayMessage(String Message){
        System.out.println(Message);
    }
    public static void initVector(long[] vector, char b){
        for (int i = 0; i <vector.length ; i++) {
            System.out.println(b+"["+i+"]=");
            vector[i] = in.nextInt();
        }
    }
    public static long[] reversedVectori(long[] vector){
        long[] arr = new long[vector.length];
        for (int i = vector.length-1; i >=0 ; i--) {
            arr[vector.length-i-1] = vector[i];
        }
        return arr;
    }
    public static void afisareVector(long[] vector){
        for (long l : vector) {
            System.out.print(l + " ");
        }
    }
    public static void main(String[] args) {
        displayMessage("Da-ti dimensiunea valorii: ");
        int dim = in.nextInt();
        long[] array = new long[dim];
        initVector(array, 'a');
        //afisareVector(reversedVectori(array));
        System.out.println();
        //secondDisplay(reversedVectori(array));
        afisareVector(reversedVectori(array));
    }
    public static void secondDisplay(long[] vector){
        for (int i = vector.length-1; i >=0 ; i--) {
            System.out.print(vector[i]+ " ");
        }
    }
    public static long[] reveseVector2(long[] vector){
        int a = vector.length%2;
        for (int i = 0; i <=a ; i++) {
            long aux = vector[i];
            vector[i] = vector[vector.length-i-1];
            vector[vector.length-i-1] = aux;
        }
        return vector;
    }
}
