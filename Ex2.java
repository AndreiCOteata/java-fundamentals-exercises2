import java.util.Scanner;

public class Ex2 {
    public static Scanner in = new Scanner(System.in);
    public static boolean sirContineSir(String h, String g){
        h=h.toLowerCase();
        g=g.toLowerCase();
        return h.contains(g);
    }
    public static boolean sirContainsInt(String h, int g){
        String str = Integer.toString(g);
        return h.contains(str);
    }
    public static boolean intregContainsIntreg(int a, int b) {
        String str1 = Integer.toString(a);
        String str2 = Integer.toString(b);
        return str1.contains(str2);
    }
    public static void checkStrings(){
        displayMessage("Da-ti primul String de la tastatura: ");
        in.nextLine();
        String l = in.nextLine();
        displayMessage("Da-ti un Sir de cautat: ");
        String intreg = in.nextLine();
        if(sirContineSir(l,intreg)){
            displayMessage("Sirul " + "\"" + l + "\" " + "contine " + "\"" + intreg + "\"");
        }else{displayMessage("Sirul " + "\"" + l + "\" " + " nu contine " + "\"" + intreg + "\"");}
    }
    public static void checkIntinString(){
        displayMessage("Da-ti primul String de la tastatura: ");
        in.nextLine();
        String l = in.nextLine();
        displayMessage("Da-ti un Integer de cautat: ");
        int intreg = in.nextInt();
        if(sirContainsInt(l,intreg)){
            displayMessage("Sirul " + "\"" + l + "\" " + "contine " + "\"" + intreg + "\"");
        }else{displayMessage("Sirul " + "\"" + l + "\" " + " nu contine " + "\"" + intreg + "\"");}
    }
    public static void checkIntinInt(){
        displayMessage("Da-ti un Intreg de la tastatura: ");
        int q = in.nextInt();
        displayMessage("Da-ti un Integer de cautat: ");
        int p = in.nextInt();
        if(intregContainsIntreg(q,p)) {
            displayMessage("Intregul " + "\"" + q + "\" " + "contine " + "\"" + p + "\"");
        }else{
            displayMessage("Intregul " + "\"" + q + "\" " + " nu contine " + "\"" + p + "\"");
        }
    }
    public static void meniuDeOptiouni(String R){
        while(R.contains("Yes")){
            displayMessage("What would you like to do? \n Please choose from the following options: ");
            displayMessage("1. String contains String \n2. String contains Integer \n3. Integer contains Integer");
            int option = in.nextInt();
            switch (option){
                case 1:
                    checkStrings(); break;
                case 2:
                    checkIntinString();
                    break;
                case 3:
                    checkIntinInt();
                    break;
                default:
                    displayMessage("Nici una din optiunile alese nu se gasesc in meniu!");
                    break;
            }
            displayMessage("If you wish to continue please type \"Yes\" (case sensitive!).");
            in.nextLine();
            R = in.nextLine();
        }
    }
    public static void displayMessage(String message){ System.out.println(message); }
    public static void main(String[] args) {
        String R = "Yes";
        meniuDeOptiouni(R);
    }
}
